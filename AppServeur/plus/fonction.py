import requests

def nouveauDeck(url):
    req = requests.get(url)
    json = req.json()
    return(json["deck_id"])

def draw(nombre,id):
    if id == "":
        rq = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
        deck = rq.json()
        id = deck["deck_id"]
    req = requests.get("https://deckofcardsapi.com/api/deck/"+id+"/draw/?count={}".format(nombre))
    json= req.json()
    res = {"deck id": id ,"cards": json["cards"]}
    return res