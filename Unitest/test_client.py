import unittest

from AppClient.main import compteNbrCarte

class TestClient(unittest.TestCase):
    def test_compteNbrCarte(self):
        carte = [
    {
      "code": "4C",
      "image": "https://deckofcardsapi.com/static/img/4C.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/4C.svg",
        "png": "https://deckofcardsapi.com/static/img/4C.png"
      },
      "value": "4",
      "suit": "CLUBS"
    },
    {
      "code": "KD",
      "image": "https://deckofcardsapi.com/static/img/KD.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/KD.svg",
        "png": "https://deckofcardsapi.com/static/img/KD.png"
      },
      "value": "KING",
      "suit": "DIAMONDS"
    },
    {
      "code": "2S",
      "image": "https://deckofcardsapi.com/static/img/2S.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/2S.svg",
        "png": "https://deckofcardsapi.com/static/img/2S.png"
      },
      "value": "2",
      "suit": "SPADES"
    },
    {
      "code": "2H",
      "image": "https://deckofcardsapi.com/static/img/2H.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/2H.svg",
        "png": "https://deckofcardsapi.com/static/img/2H.png"
      },
      "value": "2",
      "suit": "HEARTS"
    },
    {
      "code": "9H",
      "image": "https://deckofcardsapi.com/static/img/9H.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/9H.svg",
        "png": "https://deckofcardsapi.com/static/img/9H.png"
      },
      "value": "9",
      "suit": "HEARTS"
    },
    {
      "code": "3H",
      "image": "https://deckofcardsapi.com/static/img/3H.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/3H.svg",
        "png": "https://deckofcardsapi.com/static/img/3H.png"
      },
      "value": "3",
      "suit": "HEARTS"
    },
    {
      "code": "6S",
      "image": "https://deckofcardsapi.com/static/img/6S.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/6S.svg",
        "png": "https://deckofcardsapi.com/static/img/6S.png"
      },
      "value": "6",
      "suit": "SPADES"
    },
    {
      "code": "5D",
      "image": "https://deckofcardsapi.com/static/img/5D.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/5D.svg",
        "png": "https://deckofcardsapi.com/static/img/5D.png"
      },
      "value": "5",
      "suit": "DIAMONDS"
    },
    {
      "code": "4S",
      "image": "https://deckofcardsapi.com/static/img/4S.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/4S.svg",
        "png": "https://deckofcardsapi.com/static/img/4S.png"
      },
      "value": "4",
      "suit": "SPADES"
    },
    {
      "code": "JC",
      "image": "https://deckofcardsapi.com/static/img/JC.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/JC.svg",
        "png": "https://deckofcardsapi.com/static/img/JC.png"
      },
      "value": "JACK",
      "suit": "CLUBS"
    }
  ]

        self.assertEqual({'HEARTS': 3, 'SPADES': 3, 'DIAMONDS': 2, 'CLUBS': 2},compteNbrCarte(carte))

if  __name__ =='__main__': 
    unittest.main()

