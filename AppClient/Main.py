import requests

url = "http://127.0.0.1:8000"

req = requests.get(url+"/creer-un-deck/")

deck =req.json()

id_deck = deck["deck_id"]

print("Identifiant du deck : " + id_deck +"\n")


def draw(nbrdecartes,id=id_deck):
    if id == "":
        requ = requests.get(url+"/creer-un-deck/")
        id = requ.json()["deck_id"]

    deck = {"deck_id": id}
    req = requests.post(url+"/cartes/{}".format(nbrdecartes),json = deck)
    dict = req.json()
    carte = dict["cards"]
    return(carte)

def compteNbrCarte(carte):
    compte = {"HEARTS":0,"SPADES":0,"DIAMONDS":0,"CLUBS":0}
    for element in carte : 
        compte[element["suit"]]+=1
    return compte




cartes = draw(10,id_deck)

compte = compteNbrCarte(cartes)
print("♥ ♣ Liste des cartes ♦ ♠ \n")

for carte in cartes:
    print(carte['value'] + " of " + carte['suit'])

print("\n ♥ ♣ Liste des couleurs ♦ ♠ \n")

print(" ♥ : {} \n ♦ : {} \n ♣ : {} \n ♠ : {}".format(compte['HEARTS'],compte['DIAMONDS'], compte['CLUBS'], compte['SPADES']))





