# TP noté de Conception Logiciel

Bonjour et bienvenue sur ce TP de conception logiciel qui a pour but de obtenir dix carte d'un deck de jeu fournie sur l'api https://deckofcardsapi.com/

## Partie Webservice 
Via votre terminal depuis la racine : 


```sh
cd Appserveur
pip install -r requirements.txt
uvicorn Main:app --reload
```

On peut accéder à cette api via : http://127.0.0.1:8000/docs#

## Partie Client 

Après avoir lancé la partie serveur, on peut ensuite lancer la partie client, dans un autre terminal depuis la racine, rentrer : 

```sh
cd Appclient
pip install -r requirements.txt
python main.py ou python3 main.py
```

## Test Application Client

Enfin, il est possible de tester la fonction qui permet de compter la distribution des couleurs de chaque deck. Via la racine, dans un terminal : 

```sh
cd Unitest
python -m unittest test_client.py 
ou 
python3 -m unittest test_client.py
```


C'est fini pour aujourd'hui :smile: 