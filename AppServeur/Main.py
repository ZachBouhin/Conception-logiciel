import requests
from fastapi import FastAPI
from pydantic import BaseModel
from plus.fonction import *
from plus.metier import deck
app=FastAPI()


    
@app.get("/")
async def source():
    return {"Le lien de l'api est : https://deckofcardsapi.com/"}



@app.get("/creer-un-deck/")
async def getNewDeck():
    response = nouveauDeck("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    return {"deck_id" : response}


@app.post("/cartes/{nombre_cartes}")
async def drawCartes(nombre_cartes : int, Item : deck):
    res = draw(nombre_cartes, Item.id)
    return res
